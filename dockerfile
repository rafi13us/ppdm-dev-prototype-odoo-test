# Use an Odoo base image
FROM odoo:latest

# Copy your module to the Odoo addons directory
COPY ./addons/indika_module /mnt/extra-addons/indika_module

# You may need to add additional configurations or dependencies if necessary

# Expose ports if required
EXPOSE 8069 8071

# Start Odoo
CMD ["odoo", "--addons-path=/mnt/extra-addons,/usr/lib/python3/dist-packages/odoo/addons"]
